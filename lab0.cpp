#include <iostream>
#include <string>

class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx = 0.0, float yy = 0.0, float zz = 0.0) : x(xx), y(yy), z(zz) {
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "in Vector3 constructor" << std::endl;
    }

    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        std::cout << "in Vector3 destructor" << std::endl;
    }
};

Vector3 operator+(Vector3& v1, Vector3& v2){
	Vector3 v3(v1.x+v2.x,v1.y+v2.y,v1.z+v2.z);
        return v3;
}

Vector3 add(Vector3& v1, Vector3& v2){
	
	Vector3 v3(v1.x+v2.x,v1.y+v2.y,v1.z+v2.z);
	return v3;
}

std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
    stream << "("<< v.x<<","<<v.y<<","<<v.z<<")";
    return stream;
}


int main(int argc, char** argv) {
	for(int i=0;i<argc;i++){
		std::cout << argv[i] << std::endl;
	}
	
	std::string inp;
	std::cout << "Enter your name:" << std::endl;
	std::cin >> inp;
	//getline(std::cin, inp);
	std::cout << "hello " << inp << std::endl;

	Vector3 a(1,2,3);   // allocated to the stack
	Vector3 b(4,5,6);

	Vector3 c = a+b;
	std::cout<<"new Vector3: "<< c <<std::endl;


	Vector3 p7(0,0,0);
	p7.y = 5;
	std::cout<<p7<<std::endl;

	Vector3* p7a = new Vector3;
	p7a->y = 5;
	std::cout<<*p7a<<std::endl;
	delete p7a;

	Vector3 p8[10];
	Vector3* ptr = p8;

	Vector3* p8arr = new Vector3[10];
	int s = sizeof(p8[0]);
	for(int i=0;i<10;i++){
		p8arr->y = 5;
		std::cout<<"Element "<<i<<" = "<<*p8arr<<std::endl;
		p8arr+=s;
	}
}